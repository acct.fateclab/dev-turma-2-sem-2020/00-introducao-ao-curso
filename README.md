# Informações sobre o Curso
Olá Aluno :)

Este curso possui o objetivo de contemplarmos os conceitos básciso das principais tecnologias usadas para o desenvolvimento web moderno.

Além disso iremos trabalhar com tecnologias open-source, a fim da inserção do aluno dentro desse universo.

Dentro dele, iremos cobrir as seguintes partes:
- Git - Ferramenta para controle de versionamento de arquivos.
- Javascript - Linguagem de programação .
- React - Biblioteca para construção de Interfaces de Usuário (UI) criada pelo Facebook.
- NodeJS - Ambiente de desenvolvimento backend para construções de APIs modernas usando a linguagem JavaScript.
- MongoDB - Banco de dados NOSQL escalável usado para persistência de dados.

## Intrutores

- Fabio: fabio@acct.global
- Gustavo: gustavo.vasconcellos@acct.global
- Gabriel: gabriel.carvalho@acct.global

## Slack
Disponibilizaremos um canal de comunicação online que poderá ser utilizado pelos alunos para **tirar dúvidas em qualquer dia e horário da semana**. A resposta será disponibiliza em até 1 dia útil.

Dentro do Slack teremos os canais por tema (#react, #git, #javascript, #nodejs, #mongodb) e o canal de anúncios gerais (#general).

Sempre que disponibilizarmos uma nova aula ou correções faremos o anúncio notificando no canal #general do Slack.

**Link de Acesso**: [https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw](https://join.slack.com/t/acctlab/shared_invite/zt-7ch8n1sy-FlCOrJkbGoVGPvBcotdpWw)

![Slack Link](https://i.imgur.com/8udnNqJ.png)

**WhatsApp**: [https://chat.whatsapp.com/EMyAuB5Wx3u9ZswdfHpcgn](https://chat.whatsapp.com/EMyAuB5Wx3u9ZswdfHpcgn)

![Whatsapp Link](https://i.imgur.com/1o2l6zS.png)

## Programação do Curso

### Carga horária:
- Duração total do curso de 4 meses - Haverão 2 turmas por ano.
- Total de dedicação mínima semanal: 5 horas semanais (20h mensais) sendo:
-- 2 horas presenciais semanais no laboratório FATEC - Horário 17h às 19h, terças-feiras (total 8 horas mensais).
-- 3 horas extra-classe semanais - Horário definido pelo aluno.

### Primeira fase (40 horas) - FRONT-END:

#### Construção de aplicações web front-end com React UI, com ênfase em Progressive Web Apps (Google). - 10 horas
- Semana 1 (08/09) - Introdução ao Javascript e ao GIT
- Semana 2 (15/09) - Aplicações mais avançadas em Javascript e GIT

#### Construção de aplicações web front-end com React UI, com ênfase em Progressive Web Apps (Google). - 30 horas
- Semana 3 (22/09) - Introdução ao React
- Semana 4 (29/09) - Uso de Components and Props, State and Lifecycle, Handling Events,  Conditional Rendering.
- Semana 5 (06/10) - Lists and Keys, Forms, Lifting State Up, Composition vs Inheritance
- Semana 6 (13/10) - Conceitos de PWA, Networking, Requisições e `fetch`
- Semana 7 (20/10) - Hooks 
- Semana 8 (27/10) - Typescript React

### Segunda fase (40 horas) - BACK-END:

#### Construções de aplicações web back-end e API's com NODEJS e Persistência de dados em banco de dados NOSQL com MongoDB - 25 horas
- Semana 9 (03/11) - Introdução ao Node.js
- Semana 10 (10/11) - Criação de APIs com Express
- Semana 11 (17/11) - Migrando nossa API para Typescript 
- Semana 12 (24/11) - Headers e Cabeçalhos HTTP
- Semana 13 (01/12) - Introdução ao MongoDB
- Semana 14 (08/12) - Utilizando APIs consumindo o banco de dados

#### Desafio Final - 15 horas
- Semana 15 (15/12) - Entrega do Back-end e checkpoint do projeto
- Semana 16 (22/12) - Entrega do Projeto
